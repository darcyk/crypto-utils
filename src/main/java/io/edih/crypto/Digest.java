package io.edih.crypto;

public enum Digest {

    SHA_512("SHA_512"),
    SHA_256("SHA_256");

    private String digest = null;

    Digest(String digest) {
        this.digest = digest;
    }

    public String getDigest() {
        return digest;
    }
}
