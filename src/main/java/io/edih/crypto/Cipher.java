package io.edih.crypto;

public enum Cipher {

    AES_CBC("AES/CBC/PKCS7"),
    AES_GCM("AES/GCM/PKCS7");

    private String cipher = null;

    Cipher(String cipher) {
        this.cipher = cipher;
    }

    public String getCipher() {
        return cipher;
    }
}
