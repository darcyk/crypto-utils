package io.edih.crypto;

/** Factory class instance */
public class CryptoUtilsFactory {

    private static CryptoUtils factory = new CryptoUtilsImpl();

    /** Make me a singleton */
    private CryptoUtilsFactory() {
        super();
    }

    /**
     * Get the factory generated instance
     * @return CryptoUtils The factory generated instance
     */
    public static final CryptoUtils getInstance() {
        return factory;
    }
}
